var Bicicleta = require('../../models/bicicleta');

beforeEach(() => {Bicicleta.allBicis= []; });
describe('Bicicleta.allBicis', () =>{
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () =>{
    it('agreguemos una', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);

        var a= new Bicicleta(1, 'rojo', 'urbana', [-25.380738, -57.146133]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);

        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findByid', () =>{
    if('debe devolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici= new Bicicleta(1, "verde", "urbana");
        var aBici2= new Bicicleta(2, "rojo", "montaña");
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

        var targetBici= Bicicleta.findById(1);
        expert(targetBici.id).toBe(1);
        expert(targetBici.color).toBe(aBici.color);
        expert(targetBici.modelo).toBe(aBici.modelo);
    });
});